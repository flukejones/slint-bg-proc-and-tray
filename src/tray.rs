//! A seld-contained tray icon with menus. The control of app<->tray is done via
//! commands over an MPSC channel.

use std::error::Error;
use std::io::Write;
use std::time::Duration;

use gtk::gio::Icon;
use gtk::prelude::*;
use libappindicator::{AppIndicator, AppIndicatorStatus};

use crate::{get_ipc_file, SHOW_GUI};

const TRAY_APP_ICON: &str = "computer";
const TRAY_LABEL: &str = "ROG Control Center";

pub struct ROGTray {
    tray: AppIndicator,
    menu: gtk::Menu,
    icon: &'static str,
}

impl ROGTray {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        let rog_tray = Self {
            tray: AppIndicator::new(TRAY_LABEL, TRAY_APP_ICON),
            menu: gtk::Menu::new(),
            icon: TRAY_APP_ICON,
        };
        Ok(rog_tray)
    }

    pub fn set_icon(&mut self, icon: &'static str) {
        self.icon = icon;
        self.tray.set_icon(self.icon);
        self.tray.set_status(AppIndicatorStatus::Active);
    }

    /// Add a menu item with an icon to the right
    fn add_icon_menu_item<F>(&mut self, label: &str, icon: &str, cb: F)
    where
        F: Fn() + Send + 'static,
    {
        let g_box = gtk::Box::new(gtk::Orientation::Horizontal, 6);
        let icon = gtk::Image::from_gicon(&Icon::for_string(icon).unwrap(), gtk::IconSize::Menu);
        let label = gtk::Label::new(Some(label));
        let item = gtk::MenuItem::new();
        g_box.add(&icon);
        g_box.add(&label);

        item.add(&g_box);
        item.show_all();

        item.connect_activate(move |_| {
            cb();
        });
        self.menu.append(&item);
        self.menu.show_all();
        self.tray.set_menu(&mut self.menu);
    }

    fn menu_add_base(&mut self) {
        self.add_icon_menu_item("Open app", "asus_notif_red", move || {
            if let Ok(mut ipc) = get_ipc_file().map_err(|e| {
                println!("ROGTray: get_ipc_file: {}", e);
            }) {
                println!("Tray told app to show self");
                ipc.write_all(&[SHOW_GUI]).ok();
            }
        });
        self.add_icon_menu_item("Close app", "asus_notif_red", move || {
            if let Ok(mut ipc) = get_ipc_file().map_err(|e| {
                println!("ROGTray: get_ipc_file: {}", e);
            }) {
                println!("Tray told app to hide self");
                ipc.write_all(&[0]).ok();
            }
        });
    }

    /// Do a flush, build, and update of the tray menu
    fn rebuild_and_update(&mut self) {
        self.menu = gtk::Menu::new();
        self.menu_add_base();
        self.tray.set_menu(&mut self.menu);
    }
}

/// The tray is controlled somewhat by `Arc<Mutex<SystemState>>`
pub fn init_tray() {
    std::thread::spawn(move || {
        let gtk_init = gtk::init().map_err(|e| {
            println!("ROGTray: gtk init {e}");
            e
        });
        if gtk_init.is_err() {
            return;
        } // Make this the main thread for gtk

        let mut tray = match ROGTray::new() {
            Ok(t) => {
                println!("init_tray: built menus");
                t
            }
            Err(e) => {
                println!("ROGTray: tray init {e}");
                return;
            }
        };

        tray.rebuild_and_update();
        tray.set_icon(TRAY_APP_ICON);

        loop {
            if gtk::events_pending() {
                // This is blocking until any events are available
                gtk::main_iteration();
                continue;
            }
            // Don't spool at max speed if no gtk events
            std::thread::sleep(Duration::from_millis(50));
        }
    });
}
