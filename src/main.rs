use std::borrow::BorrowMut;
use std::error::Error;
use std::io::{Read, Write};
use std::thread;

use slint::ComponentHandle;
use tray_test::tray::init_tray;
use tray_test::{get_ipc_file, on_tmp_dir_exists, SHOWING_GUI};
use tray_test::{MainWindow, SHOW_GUI};

fn main() -> Result<(), Box<dyn Error>> {
    // tmp-dir must live to the end of program life
    let _tmp_dir = match tempfile::Builder::new()
        .prefix("rog-gui")
        .rand_bytes(0)
        .tempdir()
    {
        Ok(tmp) => tmp,
        Err(_) => on_tmp_dir_exists().unwrap(),
    };

    init_tray();

    thread_local! { pub static UI: std::cell::RefCell<Option<MainWindow>> = Default::default()};

    i_slint_backend_selector::with_platform(|_| Ok(())).unwrap();

    thread::spawn(move || {
        let mut buf = [0u8; 4];
        // blocks until it is read, typically the read will happen after a second
        // process writes to the IPC (so there is data to actually read)
        loop {
            if get_ipc_file().unwrap().read(&mut buf).is_ok() && buf[0] == SHOW_GUI {
                println!("Should show window {buf:?}");
                let mut ipc_file = get_ipc_file().unwrap();
                ipc_file.write_all(&[SHOWING_GUI]).unwrap();

                i_slint_core::api::invoke_from_event_loop(move || {
                    UI.with(|ui| {
                        let mut ui = ui.borrow_mut();
                        if let Some(ui) = ui.as_mut() {
                            ui.window().show().unwrap();
                            ui.window()
                                .on_close_requested(|| slint::CloseRequestResponse::HideWindow);
                        } else {
                            let newui = MainWindow::new().unwrap();
                            newui.window().show().unwrap();
                            println!("New window should be showing now"); // but it isn't
                            newui
                                .window()
                                .on_close_requested(|| slint::CloseRequestResponse::HideWindow);
                            ui.replace(newui);
                        }
                    });
                })
                .unwrap();
            } else {
                println!("Should hide window {buf:?}");
                i_slint_core::api::invoke_from_event_loop(move || {
                    UI.with(|ui| {
                        let mut ui = ui.take();
                        if let Some(ui) = ui.borrow_mut() {
                            ui.window().hide().unwrap();
                        }
                    });
                })
                .unwrap();
            }
        }
    });

    i_slint_backend_selector::with_platform(|b| {
        b.set_event_loop_quit_on_last_window_closed(false);
        b.run_event_loop()
    })
    .unwrap();

    Ok(())
}
